const {Client} = require('pg')
const prodcli = {user:'postgres',host:'aragorn.esreco.net',port:5432,database:'purchasing',max:1000000,idleTimeoutMillis:10000,connectionTimeoutMillis:10000}
const devcli = {user:'postgres',host:'10.1.0.86',port:5432,database:'purchasing',max:1000000,idleTimeoutMillis:10000,connectionTimeoutMillis:10000}
const client = new Client(prodcli)
const devClient = new Client(devcli)
async function activateClients() {
    await client.connect()
    await devClient.connect()
}
async function deactivateClients() {
    await client.end
    await devClient.end
}
activateClients()
async function getData(key, query, name) {
    let results = await client.query(query)
    let q_table = `<div onclick='"${name}"' ondblclick='showQuadrant("${key}")' class='quadrant' id='quadrant-` + key + "'><table>"
    let headers = results.fields 
    let data = results.rows 
    let col_count = headers.length 
    let row_count = data.length 
    q_table = q_table + "<thead><tr><th colspan='" + (col_count + 1).toString() + "'><p style='float:left'>" + name + ' - ' + col_count.toString() + " columns, " + row_count.toString() + " rows</p></th></tr><tr><th>#</th>"
    for(let a = 0; a < headers.length; a++){q_table = q_table + "<th>" + headers[a].name + "</th>"} 
    q_table = q_table + "</tr></thead>" 
    q_table = q_table + "<tbody>"
    for(let b = 0; b < data.length; b++) {
        q_table = q_table + "<tr><td>" + (b + 1) + '</td>';
        for(let c = 0; c < headers.length; c++) {
            if(data[b][headers[c].name] === 0){data[b][headers[c].name] = '0'}
            if(!data[b][headers[c].name]){data[b][headers[c].name] = '<i style="color:red">empty</i>'}
            if(typeof(data[b][headers[c].name]) === 'object'){data[b][headers[c].name] = JSON.stringify(data[b][headers[c].name])}
            q_table = q_table + '<td>' + data[b][headers[c].name] + '</td>'
        }
        q_table = q_table + '</tr>'
    }
    q_table = q_table + '</tbody></table></div>'
    let bytes = q_table.length
    let kilobytes = bytes / 1024
    let megabytes = kilobytes / 1024
    console.log(megabytes)
    return q_table
}
async function getDevData(key, query, name) {
    let results = await devClient.query(query)
    let q_table = `<div onclick='"${name}"' ondblclick='showQuadrant("${key}")' class='quadrant' id='quadrant-` + key + "'><table>" 
    let headers = results.fields 
    let data = results.rows 
    let col_count = headers.length 
    let row_count = data.length 
    q_table = q_table + "<thead><tr><th colspan='" + (col_count + 1).toString() + "'><p style='float:left'>" + name + ' - ' + col_count.toString() + " columns, " + row_count.toString() + " rows</p></th></tr><tr><th>#</th>"
    for(let a = 0; a < headers.length; a++){q_table = q_table + "<th>" + headers[a].name + "</th>"} 
    q_table = q_table + "</tr></thead>"
    q_table = q_table + "<tbody>"
    for(let b = 0; b < data.length; b++) {
        q_table = q_table + "<tr><td>" + (b + 1) + '</td>';
        for(let c = 0; c < headers.length; c++) {
            if(data[b][headers[c].name] === 0){data[b][headers[c].name] = '0'}
            if(!data[b][headers[c].name]){data[b][headers[c].name] = '<i style="color:red">empty</i>'}
            if(typeof(data[b][headers[c].name]) === 'object'){data[b][headers[c].name] = JSON.stringify(data[b][headers[c].name])}
            q_table = q_table + '<td>' + data[b][headers[c].name] + '</td>'
        }
        q_table = q_table + '</tr>'
    }
    q_table = q_table + '</tbody></table></div>'
    let bytes = q_table.length
    let kilobytes = bytes / 1024
    let megabytes = kilobytes / 1024
    console.log(megabytes)
    return q_table
}
async function getGroup(groupId) {
    let queries = await client.query(`select * from jsreport_queries where group_key = '${groupId}'`)
    let table = ''
    let new_table = ''
    for(let i = 0; i < queries.rows.length; i++) {
        new_table = await getData(queries.rows[i].key, queries.rows[i].query, queries.rows[i].name)
        table = table + new_table
    }
    return table
}
async function getGroupDev(groupId) {
    let queries = await devClient.query(`select * from jsreport_queries where group_key = '${groupId}'`)
    let table = ''
    let new_table = ''
    for(let i = 0; i < queries.rows.length; i++) {
        new_table = await getDevData(queries.rows[i].key, queries.rows[i].query, queries.rows[i].name)
        table = table + new_table
    }
    return table
}
async function getItem(itemId) {
    let queries = await client.query(`select * from jsreport_queries where key = '${itemId}'`)
    let table = ''
    let new_table = ''
    for(let i = 0; i < queries.rows.length; i++) {
        new_table = await getData(queries.rows[i].key, queries.rows[i].query, queries.rows[i].name)
        table = table + new_table
    }
    return table
}
async function getItemDev(itemId) {
    let queries = await devClient.query(`select * from jsreport_queries where key = '${itemId}'`)
    let table = ''
    let new_table = ''
    for(let i = 0; i < queries.rows.length; i++) {
        new_table = await getDevData(queries.rows[i].key, queries.rows[i].query, queries.rows[i].name)
        table = table + new_table
    }
    return table
}
async function getTitle(groupId) {
    if(groupId) {groupId = groupId.toString()}
    let query = `select group_name from jsreport_queries where group_key = '${groupId}' limit 1`
    let group = await client.query(query)
    let title = 'JSReport'
    if(group.rows[0]) {if(group.rows[0].group_name) {title = group.rows[0].group_name.toString() + ' - JSReport'}}
    return title
}
async function getTitleDev(groupId) {
    if(groupId) {groupId = groupId.toString()}
    let query = `select group_name from jsreport_queries where group_key = '${groupId}' limit 1`
    let group = await devClient.query(query)
    let title = 'JSReport'
    if(group.rows[0]) {if(group.rows[0].group_name) {title = group.rows[0].group_name.toString() + ' - JSReport'}}
    return title
}
async function getITitle(groupId) {
    if(groupId) {groupId = groupId.toString()}
    let query = `select name from jsreport_queries where key = '${groupId}' limit 1`
    let group = await client.query(query)
    let title = 'JSReport'
    if(group.rows[0]) {if(group.rows[0].name) {title = group.rows[0].name.toString() + ' - JSReport'}}
    return title
}
async function getITitleDev(groupId) {
    if(groupId) {groupId = groupId.toString()}
    let query = `select name from jsreport_queries where key = '${groupId}' limit 1`
    let group = await devClient.query(query)
    let title = 'JSReport'
    if(group.rows[0]) {if(group.rows[0].name) {title = group.rows[0].name.toString() + ' - JSReport'}}
    return title
}
async function getGroups(env) {
    if(env == 'prod') {
        const query = `select distinct(group_key) from jsreport_queries`
        const results = await client.query(query)
        const data = results.rows
        let list = ''
        for(let i = 0; i < data.length; i++) {if(data[i].group_key) {list = list + `<option id='group-${data[i].group_key.toString()}' class='group-opt' value='${data[i].group_key.toString()}'>${await getGroupName(env, data[i].group_key.toString())}</option>`}}
        return list
    }
    else {
        const query = `select distinct(group_key) from jsreport_queries`
        const results = await devClient.query(query)
        const data = results.rows
        let list = ''
        for(let i = 0; i < data.length; i++) {if(data[i].group_key) {list = list + `<option id='group-${data[i].group_key.toString()}' class='group-opt' value='${data[i].group_key.toString()}'>${await getGroupName(env, data[i].group_key.toString())}</option>`}}
        return list
    }
}
async function getItems(env) {
    if(env == 'prod') {
        const query = `select key from jsreport_queries`
        const results = await client.query(query)
        const data = results.rows
        let list = ''
        for(let i = 0; i < data.length; i++) {
            if(data[i].key) {
                list = list + `<option id='item-${data[i].key.toString()}' class='item-opt' value='${data[i].key.toString()}'>${await getItemName(env, data[i].key.toString())}</option>`
            }
        }
        return list
    }
    else {
        const query = `select key from jsreport_queries`
        const results = await devClient.query(query)
        const data = results.rows
        let list = ''
        for(let i = 0; i < data.length; i++) {
            if(data[i].key) {
                list = list + `<option id='item-${data[i].key.toString()}' class='item-opt' value='${data[i].key.toString()}'>${await getItemName(env, data[i].key.toString())}</option>`
            }
        }
        return list
    }
}
async function getGroupName(env, key) {
    if(env == 'prod') {
        const query = `select group_name from jsreport_queries where group_key = '${key}' limit 1`
        const results = await client.query(query)
        const data = results.rows
        let groupName = 'Unknown'
        if(data[0]) {if(data[0].group_name) {groupName = data[0].group_name.toString()}}
        return groupName
    }
    else {
        const query = `select group_name from jsreport_queries where group_key = '${key}' limit 1`
        const results = await devClient.query(query)
        const data = results.rows
        let groupName = 'Unknown'
        if(data[0]) {if(data[0].group_name) {groupName = data[0].group_name.toString()}}
        return groupName
    }
}
async function getItemName(env, key) {
    if(env == 'prod') {
        const query = `select name from jsreport_queries where key = '${key}' limit 1`
        const query1 = `select group_name from jsreport_queries where key = '${key}' limit 1`
        const results = await client.query(query)
        const results1 = await client.query(query1)
        const data = results.rows
        const data1 = results1.rows
        let itemName = 'Unknown'
        if(data[0]) {if(data[0].name) {itemName = data[0].name.toString()}}
        if(data1[0]) {if(data1[0].group_name) {itemName = itemName + ' - ' + data1[0].group_name.toString()}}
        return itemName
    }
    else {
        const query = `select name from jsreport_queries where key = '${key}' limit 1`
        const query1 = `select group_name from jsreport_queries where key = '${key}' limit 1`
        const results = await devClient.query(query)
        const results1 = await devClient.query(query1)
        const data = results.rows
        const data1 = results1.rows
        let itemName = 'Unknown'
        if(data[0]) {if(data[0].name) {itemName = data[0].name.toString()}}
        if(data1[0]) {if(data1[0].group_name) {itemName = itemName + ' - ' + data1[0].group_name.toString()}}
        return itemName
    }
}
deactivateClients()