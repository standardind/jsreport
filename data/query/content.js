function beforeRender(req, res, done) {
    if(req.context.http.query.type){req.data.reportType = req.context.http.query.type.toString()}
    else{req.data.reportType = 'Undefined'}
    if(req.context.http.query.item){req.data.reportItem = req.context.http.query.item.toString()}
    else{req.data.reportItem = 'Undefined'}
    if(req.context.http.query.group){req.data.reportGroup = req.context.http.query.group.toString()}
    else{req.data.reportGroup = 'Undefined'}
    if(req.context.http.query.env){req.data.reportEnv = req.context.http.query.env.toString()}
    else{req.data.reportEnv = 'Undefined'}
    done()
}