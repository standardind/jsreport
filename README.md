# JSReport Server

Prerequisites: NodeJS, NPM Package Manager, Git, JSReport

```bash
sudo (apt-get / pacman) (install / -S) nodejs npm git
sudo npm i -g jsreport-cli
sudo npm i -g jsreport
sudo npm i -g phantom-pdf
sudo npm i -g jsreport-phantom-pdf
```

## For Linux / Mac

```bash
git clone 'repo-link'
cd 'repo-name'
chmod +x install
chmod +x start
./install
```

## For windows

```bash
git clone 'repo-link'
cd 'repo-name'
call install.bat
```

## To Start Post-Installation
### Linux
```bash
./start
```
or
```bash
npm start
```
### Windows
```bash
call start.bat
```
or
```bash
jsreport start
```